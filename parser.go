package main

import (
	"log"
	"niki"
	"os"
)

func main() {
	fname := os.Args[1]
	file, err := os.Open(fname)
	if err != nil {
		log.Fatalf("Could not open %s", fname)
	}

	program, err := niki.Compile(file)
	if err != nil {
		log.Fatalf("Could not load program from %s: %s", fname, err)
	}

	// TODO: w := niki.LoadWorld(filename)
	w := niki.NewWorld(10, 10)
	w.Set(0, 0)
	w.Set(0, 0)
	// w.DisplayCells()
	n := niki.CreateRobot(w)

	vm := niki.NewVM()
	vm.Load(program)
	vm.Start(n)
	// vm.Step()
	vm.Run()
	w.DisplayCells(n)
}
