package niki

import (
	"fmt"
	"io"
)

type Parser struct {
	s   *Scanner
	buf struct {
		tok Token
		lit string
		n   int
	}
	symbols map[string]bool
}

// NewParser creates a new parser for the input reader
func NewParser(r io.Reader) *Parser {
	return &Parser{
		s:       NewScanner(r),
		symbols: make(map[string]bool)}
}

func (p *Parser) scan() (tok Token, lit string) {
	if p.buf.n > 0 {
		p.buf.n--
	} else {
		p.buf.tok, p.buf.lit = p.s.Scan()
	}
	return p.buf.tok, p.buf.lit
}

func (p *Parser) unscan() {
	p.buf.n++
}

func (p *Parser) scanIgnoreWhitespace() (tok Token, lit string) {
	tok, lit = p.scan()
	if tok == WS {
		tok, lit = p.scan()
	}
	return
}

func (p *Parser) knownIdentifier(id string) bool {
	return p.symbols[id]
}

func isCommand(tok Token) bool {
	return tok == DREHE_LINKS || tok == GEHE_VOR || tok == NIMM_AUF || tok == GIB_AB
}

func isCondition(tok Token) bool {
	return tok == VORNE_FREI || tok == PLATZ_BELEGT || tok == RECHTS_FREI || tok == LINKS_FREI
}

func (p *Parser) parseCondition() (*Condition, error) {
	cond := Condition{}
	tok, lit := p.scanIgnoreWhitespace()
	if tok == NOT {
		tok, lit = p.scanIgnoreWhitespace()
		if !isCondition(tok) {
			return nil, fmt.Errorf("Expected CONDITION, got %q", lit)
		}
		cond.Not = true
		cond.Cond = lit
	} else if isCondition(tok) {
		cond.Cond = lit
	} else {
		return nil, fmt.Errorf("Expected NOT or CONDITION, got %q", lit)
	}
	return &cond, nil
}

func (p *Parser) parseBody() ([]Statement, error) {
	var stmts []Statement
	tok, lit := p.scanIgnoreWhitespace()
	if tok != BEGIN {
		return nil, fmt.Errorf("Expected BEGIN, found %q", lit)
	}

	for {
		// Stray semicolon after last statement, before END
		tok, _ = p.scanIgnoreWhitespace()
		if tok == END {
			p.unscan()
			break
		}
		p.unscan()

		stmt, err := p.parseStatement()
		if err != nil {
			return nil, err
		}
		stmts = append(stmts, stmt)

		tok, _ = p.scanIgnoreWhitespace()
		if tok != SEMICOLON {
			p.unscan()
			break
		}
	}

	tok, lit = p.scanIgnoreWhitespace()
	if tok != END {
		return nil, fmt.Errorf("Expected END, found %q", lit)
	}

	return stmts, nil
}

func (p *Parser) parseBlock() ([]Statement, error) {
	tok, _ := p.scanIgnoreWhitespace()
	if tok == BEGIN {
		p.unscan()
		body, err := p.parseBody()
		if err != nil {
			return nil, err
		}
		return body, nil
	}
	p.unscan()
	stmt, err := p.parseStatement()
	if err != nil {
		return nil, err
	}
	return []Statement{stmt}, nil
}

func (p *Parser) parseWhile() (*WhileStmt, error) {
	cond, err := p.parseCondition()
	if err != nil {
		return nil, err
	}
	tok, lit := p.scanIgnoreWhitespace()
	if tok != DO {
		return nil, fmt.Errorf("Expected DO, found %q", lit)
	}
	body, err := p.parseBlock()
	if err != nil {
		return nil, err
	}
	stmt := WhileStmt{Cond: *cond, Body: body}
	return &stmt, nil
}

func (p *Parser) parseRepeat() (*RepeatStmt, error) {
	body, err := p.parseBlock()
	if err != nil {
		return nil, err
	}

	tok, lit := p.scanIgnoreWhitespace()
	if tok != UNTIL {
		return nil, fmt.Errorf("Expected UNTIL, found %q", lit)
	}

	cond, err := p.parseCondition()
	if err != nil {
		return nil, err
	}
	stmt := RepeatStmt{Cond: *cond, Body: body}
	return &stmt, nil
}

func (p *Parser) parseIf() (*IfStmt, error) {
	cond, err := p.parseCondition()
	if err != nil {
		return nil, err
	}

	tok, lit := p.scanIgnoreWhitespace()
	if tok != THEN {
		return nil, fmt.Errorf("Expected THEN, found %q", lit)
	}

	body, err := p.parseBlock()
	if err != nil {
		return nil, err
	}
	stmt := IfStmt{Cond: *cond, Body: body}

	tok, _ = p.scanIgnoreWhitespace()
	if tok != ELSE {
		p.unscan()
	} else {
		elseBody, err := p.parseBlock()
		if err != nil {
			return nil, err
		}
		stmt.ElseBody = elseBody
	}

	return &stmt, nil
}

func (p *Parser) parseStatement() (Statement, error) {

	tok, lit := p.scanIgnoreWhitespace()
	if tok == IF {
		ifStmt, err := p.parseIf()
		if err != nil {
			return nil, err
		}
		return ifStmt, nil
	} else if tok == WHILE {
		whileStmt, err := p.parseWhile()
		if err != nil {
			return nil, err
		}
		return whileStmt, nil
	} else if tok == REPEAT {
		repeatStmt, err := p.parseRepeat()
		if err != nil {
			return nil, err
		}
		return repeatStmt, nil
	} else if isCommand(tok) {
		stmt := SimpleStmt{Command: lit}
		return &stmt, nil
	} else if tok == IDENT {
		if !p.knownIdentifier(lit) {
			return nil, fmt.Errorf("Unknown identifier %q", lit)
		}
		stmt := CallStmt{Proc: lit}
		return &stmt, nil
	}

	return nil, fmt.Errorf("Expected Statement, got %q", lit)
}

func (p *Parser) parseProcedure() (*Procedure, error) {
	tok, lit := p.scanIgnoreWhitespace()
	if tok != IDENT {
		return nil, fmt.Errorf("Expected IDENT, found %q", lit)
	}
	body, err := p.parseBody()
	if err != nil {
		return nil, err
	}
	proc := Procedure{Name: lit, Body: body}
	return &proc, nil
}

// Parse parses the input file
func (p *Parser) Parse() (*Program, error) {

	tok, lit := p.scanIgnoreWhitespace()
	if tok != PROGRAM {
		return nil, fmt.Errorf("Expected PROGRAM, found %q", lit)
	}
	tok, lit = p.scanIgnoreWhitespace()
	if tok != IDENT {
		return nil, fmt.Errorf("Expected IDENT, found %q", lit)
	}
	prog := Program{Name: lit}

	tok, lit = p.scanIgnoreWhitespace()
	if tok != SEMICOLON {
		return nil, fmt.Errorf("Expected ';', found %q", lit)
	}

	for {
		tok, _ = p.scanIgnoreWhitespace()
		if tok == PROCEDURE {
			proc, err := p.parseProcedure()
			if err != nil {
				return nil, err
			}
			prog.Procs = append(prog.Procs, *proc)
			p.symbols[proc.Name] = true
		} else if tok != SEMICOLON {
			p.unscan()
			break
		}
	}

	// Main function
	body, err := p.parseBody()
	if err != nil {
		return nil, err
	}
	tok, lit = p.scanIgnoreWhitespace()
	if tok != DOT {
		return nil, fmt.Errorf("Expected '.', found %q", lit)
	}

	prog.main = Procedure{Name: "main", Body: body}
	return &prog, nil
}
