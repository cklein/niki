package niki

import (
	"fmt"
	"log"
)

const (
	// Invalid instruction
	INV int = iota - 1

	// Halt
	HLT

	// Robot instructions
	INC
	DEC
	ROT
	MOV

	// Robot sensor functions
	TST
	WALL

	NEG

	// Jumps, conditional and unconditional
	JMP
	JE
	JZ

	// Subroutine instructions
	CALL
	RET
)

// Job that runs on the virtual machine
type Job struct {
	Name       string
	Code       []int
	Entrypoint int
}

type VirtualMachine struct {
	Prog  []int // Instructions
	Stack []int // Nur für Rücksprungadressen

	// Registers:
	Ip      int
	Sp      int
	C       int // conditional register
	Running bool

	niki *Niki
}

// NewVM creates a new Virtual Machine.
func NewVM() *VirtualMachine {
	return &VirtualMachine{
		Stack: make([]int, 0x1000),
		niki:  &Niki{},
	}
}

func (vm *VirtualMachine) fetch() (int, bool) {
	if vm.Ip >= len(vm.Prog) {
		return INV, false
	}
	instr := vm.Prog[vm.Ip]
	vm.Ip++
	return instr, true
}

func (vm *VirtualMachine) eval(instr int) {
	switch instr {
	case HLT:
		vm.Running = false

	// Niki specific opcodes
	case INC:
		if !vm.niki.Pick() {
			log.Fatal("could not pick")
		}
	case DEC:
		if !vm.niki.Drop() {
			log.Fatal("could not drop")
		}
	case ROT:
		vm.niki.Turn()
	case MOV:
		if !vm.niki.Move() {
			log.Fatal("could not move")
		}
	case TST:
		if vm.niki.Look() > 0 {
			vm.C = 1
		} else {
			vm.C = 0
		}
	case WALL:
		if vm.niki.CanMove() {
			vm.C = 1
		} else {
			vm.C = 0
		}

	case NEG:
		vm.C = (vm.C + 1) % 2

	case JMP:
		vm.Ip++
		addr := vm.Prog[vm.Ip]
		vm.Ip = addr
	case JE:
		vm.Ip++
		addr := vm.Prog[vm.Ip]
		if vm.C > 0 {
			vm.Ip = addr
		}
	case JZ:
		vm.Ip++
		addr := vm.Prog[vm.Ip]
		if vm.C == 0 {
			vm.Ip = addr
		}
	case CALL:
		vm.Stack[vm.Sp] = vm.Ip + 2
		vm.Sp++
		addr := vm.Prog[vm.Ip]
		vm.Ip = addr
	case RET:
		vm.Ip = vm.Stack[vm.Sp]
		vm.Sp--
	case INV:
		fallthrough
	default:
		log.Fatalf("Illegal instruction: %v\n", instr)
	}
}

func (vm *VirtualMachine) Load(j *Job) {
	vm.Prog = j.Code
	vm.Ip = j.Entrypoint
}

// Start the Virtual Machine.
func (vm *VirtualMachine) Start(n *Niki) {
	vm.niki = n
	vm.Running = true
}

// Step executes the next step of the program.
func (vm *VirtualMachine) Step() {
	instr, ok := vm.fetch()
	if !ok {
		log.Fatalf("Could not fetch at Ip %x", vm.Ip)
	}
	vm.eval(instr)
}

// Run starts the Virtual Machine.
func (vm *VirtualMachine) Run() {
	for {
		if !vm.Running {
			break
		}
		vm.Step()
	}
	fmt.Println("Exit.")
}
