package niki

import (
	"fmt"
	"io"
)

type direction uint8

const (
	Right direction = iota
	Up
	Left
	Down
)

func (d direction) String() (s string) {
	switch d {
	case Right:
		s = ">"
	case Up:
		s = "^"
	case Left:
		s = "<"
	case Down:
		s = "v"
	}
	return
}

// Niki is a robot.
type Niki struct {
	dir     direction
	x, y    int
	storage int
	world   *World
}

// CreateRobot creates a new Niki instance with the given world.
func CreateRobot(w *World) *Niki {
	return &Niki{world: w}
}

// World represents the world in which our little hero operates.
type World struct {
	rows, cols int
	walls      []int
	cells      []int
}

// NewWorld creates a new world with the given dimensions.
func NewWorld(rows, cols int) *World {
	walls := make([]int, rows*cols)

	for col := 0; col < cols; col++ {
		walls[col] = walls[col] | (2 << Up)
		walls[col+cols*(rows-1)] = walls[col] | (2 << Down)
	}

	for row := 0; row < rows; row++ {
		walls[row*cols] = walls[row*cols] | (2 << Left)
		walls[row*cols+cols-1] = walls[row*cols+cols-1] | (2 << Right)
	}

	return &World{
		rows:  rows,
		cols:  cols,
		walls: walls,
		cells: make([]int, rows*cols),
	}
}

// LoadWorld loads a world.
func LoadWorld(r io.Reader) {
	// TODO
}

// The function pathExists determines, if there exists a pathfrom given position
// in given directory.
func (w *World) pathExists(row, col int, dir direction) bool {
	val := w.walls[row*w.cols+col]
	mask := 2 << uint8(dir)
	return val&mask == 0
}

// Get decrements the given location.
// Returns if the operation was successful.
func (w *World) Get(row, col int) bool {
	cell := &(w.cells[row*w.cols+col])
	if *cell > 0 {
		*cell--
		return true
	}
	return false
}

// Set increments the given location.
func (w *World) Set(row, col int) {
	w.cells[row*w.cols+col]++
}

func (w *World) DisplayCells(n *Niki) {
	for row := 0; row < w.rows; row++ {
		for col := 0; col < w.cols; col++ {
			// walls...
			wall := w.walls[row*w.cols+col]
			mask := 2 << Up
			if wall&mask == mask {
				fmt.Print("---")
			} else {
				fmt.Print("   ")
			}
		}
		fmt.Println("")

		for col := 0; col < w.cols; col++ {
			wall := w.walls[row*w.cols+col]
			mask := 2 << Left
			if wall&mask == mask {
				fmt.Print("|")
			} else {
				fmt.Print(" ")
			}

			fmt.Printf("%d ", w.cells[row*w.cols+col])
		}

		if row == 1 {
			fmt.Printf("  Pos: %d:%d", n.x, n.y)
		} else if row == 2 {
			fmt.Printf("  Direction: %s", n.dir)
		}
		fmt.Println("")
	}
	fmt.Println("")
}

// Pick picks up an item at the current position.
// Returns wether the operation was successful.
func (n *Niki) Pick() bool {
	if n.world.Get(n.x, n.y) {
		n.storage++
		return true
	}
	return false
}

// Look returns the value at the current location.
func (n *Niki) Look() int {
	return n.world.cells[n.x*n.world.cols+n.y]
}

// Drop drops an item at the current position.
// Returns wether the operation was successful.
func (n *Niki) Drop() bool {
	if n.storage > 0 {
		n.world.Set(n.x, n.y)
		n.storage--
		return true
	}
	return false
}

// Move moves niki one cell in the current direction.
// Returns wether the operation was successful.
func (n *Niki) Move() bool {
	// TODO: check if niki is going to fall off the board.
	if n.world.pathExists(n.x, n.y, n.dir) {
		switch n.dir {
		case Left:
			n.x--
		case Right:
			n.x++
		case Up:
			n.y--
		case Down:
			n.y++
		}
		return true
	}
	return false
}

// Turn turns niki 90 degrees clockwise.
func (n *Niki) Turn() {
	n.dir = (n.dir + 1) % 4
}

// CanMove if niki is able to move forward in the current direction.
func (n *Niki) CanMove() bool {
	return n.world.pathExists(n.x, n.y, n.dir)
}
