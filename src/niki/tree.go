package niki

import "fmt"

// Statement is the common interface for all statements
type Statement interface {
	fmt.Stringer
}

type Condition struct {
	Not  bool
	Cond string
}

type SimpleStmt struct {
	Statement
	Command string
}

type CallStmt struct {
	Statement
	Proc string
}

type WhileStmt struct {
	Statement
	Cond Condition
	Body []Statement
}

type RepeatStmt struct {
	Statement
	Cond Condition
	Body []Statement
}

type IfStmt struct {
	Statement
	Cond     Condition
	Body     []Statement
	ElseBody []Statement
}

type Procedure struct {
	Name string
	Body []Statement
}

type Program struct {
	Name  string
	Procs []Procedure
	main  Procedure
}

// String methods, complying the Stringer interface.
func (cond Condition) String() string {
	if cond.Not {
		return fmt.Sprintf("NOT %s", cond.Cond)
	}
	return cond.Cond
}

func (s SimpleStmt) String() string {
	return s.Command
}

func (s CallStmt) String() string {
	return s.Proc
}

func (s WhileStmt) String() string {
	return fmt.Sprintf("WHILE %s DO %s", s.Cond, s.Body)
}

func (s RepeatStmt) String() string {
	return fmt.Sprintf("REPEAT %s UNTIL %s", s.Body, s.Cond)
}

func (s IfStmt) String() string {
	return fmt.Sprintf("IF %s THEN %s", s.Cond, s.Body)
}
