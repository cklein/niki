package niki

import (
	"bufio"
	"bytes"
	"io"
	"strings"
)

// Token is the datatype for the lexer tokens.
type Token int

// Geil wäre, hier automatisch eine Map bauen zu lassen...
const (
	ILLEGAL Token = iota
	EOF
	WS

	IDENT

	START_COMMENT
	END_COMMENT
	SEMICOLON
	DOT

	// Keywords
	PROGRAM
	PROCEDURE
	IF
	THEN
	ELSE
	WHILE
	DO
	REPEAT
	UNTIL
	BEGIN
	END
	NOT

	// Aktionen
	DREHE_LINKS
	GEHE_VOR
	NIMM_AUF
	GIB_AB

	// Abfragen
	PLATZ_BELEGT
	VORNE_FREI
	LINKS_FREI
	RECHTS_FREI
)

func isWhitespace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n'
}

func isLetter(ch rune) bool {
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
}

func isDigit(ch rune) bool {
	return ch >= '0' && ch <= '9'
}

var eof = rune(0)

type Scanner struct {
	r *bufio.Reader
}

func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

func (s *Scanner) unread() { _ = s.r.UnreadRune() }

func (s *Scanner) Scan() (tok Token, lit string) {
	// Read the next rune.
	ch := s.read()

	// If we see whitespace then consume all contiguous whitespace.
	// If we see a letter then consume as an ident or reserved word.
	if isWhitespace(ch) {
		s.unread()
		return s.scanWhitespace()
	} else if isLetter(ch) {
		s.unread()
		return s.scanIdent()
	}

	// Otherwise read the individual character.
	switch ch {
	case eof:
		return EOF, ""
	case ';':
		return SEMICOLON, string(ch)
	case '.':
		return DOT, string(ch)
	case '{':
		return START_COMMENT, string(ch)
	case '}':
		return END_COMMENT, string(ch)
	}

	return ILLEGAL, string(ch)
}

func (s *Scanner) scanWhitespace() (tok Token, lit string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())
	for {
		ch := s.read()
		if ch == eof {
			break
		} else if !isWhitespace(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	return WS, buf.String()
}

func (s *Scanner) scanIdent() (tok Token, lit string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())
	for {
		ch := s.read()
		if ch == eof {
			break
		} else if !isLetter(ch) && !isDigit(ch) && ch != '_' {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}

	lit = buf.String()
	switch strings.ToUpper(lit) {
	case "PROGRAM":
		return PROGRAM, lit
	case "PROCEDURE":
		return PROCEDURE, lit
	case "IF":
		return IF, lit
	case "THEN":
		return THEN, lit
	case "ELSE":
		return ELSE, lit
	case "WHILE":
		return WHILE, lit
	case "DO":
		return DO, lit
	case "REPEAT":
		return REPEAT, lit
	case "UNTIL":
		return UNTIL, lit
	case "BEGIN":
		return BEGIN, lit
	case "END":
		return END, lit
	case "DREHE_LINKS":
		return DREHE_LINKS, lit
	case "GEHE_VOR":
		return GEHE_VOR, lit
	case "NIMM_AUF":
		return NIMM_AUF, lit
	case "GIB_AB":
		return GIB_AB, lit
	case "NOT":
		return NOT, lit
	case "VORNE_FREI":
		return VORNE_FREI, lit
	case "PLATZ_BELEGT":
		return PLATZ_BELEGT, lit
	case "RECHTS_FREI":
		return RECHTS_FREI, lit
	case "LINKS_FREI":
		return LINKS_FREI, lit
	}

	return IDENT, lit
}
