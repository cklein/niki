package niki

import (
	"fmt"
	"log"
	"strings"
)

// Emitter emits the code for a parsed program.
type Emitter struct {
	labelCnt int
	Symbols  map[string]int
	tokens   []string
}

// NewEmitter creates a new emitter object.
func NewEmitter() *Emitter {
	return &Emitter{
		Symbols: make(map[string]int),
	}
}

func (e *Emitter) createLabel() string {
	e.labelCnt++
	return fmt.Sprintf("label_%x", e.labelCnt)
}

func (e *Emitter) emitLabel(label string) {
	e.tokens = append(e.tokens, label+":")
}

func (e *Emitter) emitCond(cond Condition) {
	switch cond.Cond {
	case "PLATZ_BELEGT":
		e.emit("TST")
	case "VORNE_FREI":
		e.emit("WALL")
	}
	if cond.Not {
		e.emit("NEG")
	}
}

func (e *Emitter) emitWhileStmt(stmt *WhileStmt) {
	label1 := e.createLabel()
	e.emitLabel(label1)
	e.emitCond(stmt.Cond)
	label2 := e.createLabel()
	e.emit("JZ %s", label2)
	e.emitBlock(stmt.Body)
	e.emit("JMP %s", label1)
	e.emitLabel(label2)
}

func (e *Emitter) emitRepeatStmt(stmt *RepeatStmt) {
	label := e.createLabel()
	e.emitLabel(label)
	e.emitBlock(stmt.Body)
	e.emitCond(stmt.Cond)
	e.emit("JE %s", label)
}

func (e *Emitter) emitIfStmt(stmt *IfStmt) {
	e.emitCond(stmt.Cond)
	label1 := e.createLabel()
	e.emit("JZ %s", label1)
	e.emitBlock(stmt.Body)
	label2 := e.createLabel()
	e.emit("JMP %s", label2)
	e.emitLabel(label1)
	e.emitBlock(stmt.ElseBody)
	e.emitLabel(label2)
}

func (e *Emitter) emitSimpleStmt(stmt *SimpleStmt) {
	switch stmt.Command {
	case "GEHE_VOR":
		e.emit("MOV")
	case "DREHE_LINKS":
		e.emit("ROT")
	case "NIMM_AUF":
		e.emit("INC")
	case "GIB_AB":
		e.emit("DEC")
	}
}

func (e *Emitter) emitCallStmt(stmt *CallStmt) {
	e.emit("CALL %s", stmt.Proc)
}

func (e *Emitter) emitBlock(block []Statement) {
	for _, stmt := range block {
		e.emitStatement(stmt)
	}
}

func (e *Emitter) emitStatement(stmt Statement) {
	switch t := stmt.(type) {
	case *WhileStmt:
		e.emitWhileStmt(t)
	case *RepeatStmt:
		e.emitRepeatStmt(t)
	case *IfStmt:
		e.emitIfStmt(t)
	case *CallStmt:
		e.emitCallStmt(t)
	case *SimpleStmt:
		e.emitSimpleStmt(t)
	default:
		fmt.Printf("%T: %v\n", t, t)
	}
}

func (e *Emitter) emitProcedure(proc *Procedure) {
	// e.Symbols[proc.Name] = e.addr
	e.emitLabel(proc.Name)
	e.emitBlock(proc.Body)
	e.emit("RET")
}

// EmitProgram erzeugt den Code für ein Program
func (e *Emitter) EmitProgram(program *Program) {
	// Main entry point
	e.emitBlock(program.main.Body)
	e.emit("HLT")

	for _, proc := range program.Procs {
		e.emitProcedure(&proc)
	}
}

func (e *Emitter) emit(symbol string, params ...interface{}) {
	tok := fmt.Sprintf(symbol, params...)
	e.tokens = append(e.tokens, tok)
	// fmt.Println(tok)
}

func translate(opcode string) int {
	switch opcode {
	case "HLT":
		return HLT
	case "INC":
		return INC
	case "DEC":
		return DEC
	case "ROT":
		return ROT
	case "MOV":
		return MOV
	case "TST":
		return TST
	case "WALL":
		return WALL
	case "NEG":
		return NEG
	case "JMP":
		return JMP
	case "JE":
		return JE
	case "JZ":
		return JZ
	case "CALL":
		return CALL
	case "RET":
		return RET
	default:
		log.Fatalf("Got %s\n", opcode)
		return INV
	}
}

// Translate to bytecode
func (e *Emitter) Translate() []int {
	var bytecode []int

	symbolTable := make(map[string]int)
	addr := 0
	for _, tok := range e.tokens {
		if tok[len(tok)-1] == ':' {
			sym := tok[:len(tok)-1]
			symbolTable[sym] = addr
		} else {
			addr++
		}
		fmt.Println(tok)
	}

	for _, tok := range e.tokens {
		toks := strings.Split(tok, " ")
		if len(toks) == 2 {
			sym := toks[1]
			// len(tok)-1
			addr, ok := symbolTable[sym]
			if !ok {
				log.Fatalf("could not find symbol %v\n", sym)
			}
			bytecode = append(bytecode, translate(toks[0]))
			bytecode = append(bytecode, addr)
		} else if tok[len(tok)-1] != ':' {
			bytecode = append(bytecode, translate(tok))
		}
	}

	return bytecode
}
